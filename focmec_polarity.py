import matplotlib.pyplot as plt
import scipy.stats as ss
import numpy as np
from obspy.imaging.beachball import beach

# data
strike = 160 + 180
dip = 30
# dip = 60 # from Renier inversion
rake_ss = 180
rake_inv = 90
rake_oblique = 135
# rake_oblique = 110 # from Renier inversion
radius = 50
npts = 100

facecolor = [0.6, 0.6, 0.6]
bgcolor = [0.9, 0.9, 0.9]

# plotting points
def find_xy_from_takeoff_backaz(radius, takeoff, backaz):
    r = radius * takeoff / 90.0
    theta = backaz
    x = r * np.sin(theta*np.pi/180.)
    y = r * np.cos(theta*np.pi/180.)
    return x, y


def get_spread_from_takeoff_backaz_unc(takeoff, tunc, backaz, unc, npts):
    n = len(takeoff)
    takeoff_spread_list = []
    takeoff_prob_list = []
    backazimuth_spread_list = []
    backazimuth_prob_list = []
    for i in range(n):
        t_vals = ss.norm(takeoff[i], tunc[i]).rvs(npts)
        t_probs = ss.norm(takeoff[i], tunc[i]).pdf(t_vals)
        takeoff_spread_list.append(t_vals)
        takeoff_prob_list.append(t_probs)

        b_vals = ss.norm(backaz[i], unc[i]).rvs(npts)
        b_probs = ss.norm(backaz[i], unc[i]).pdf(b_vals)
        backazimuth_spread_list.append(b_vals)
        backazimuth_prob_list.append(b_probs)

        takeoff_spread = np.concatenate(takeoff_spread_list)
        takeoff_probs = np.concatenate(takeoff_prob_list)
        backazimuth_spread = np.concatenate(backazimuth_spread_list)
        backazimuth_probs = np.concatenate(backazimuth_prob_list)

    return takeoff_spread, takeoff_probs, backazimuth_spread, backazimuth_probs


polarities = np.array([1, 0, 1, 1, 1, 1, 1, 0, 1, 0])
back_azimuths = np.array([45, 45, 45, 45, 45, 45, 45, 45, 45, 45])
az_unc = np.array([5, 6, 12, 4, 12, 23, 8, 4, 11, 6])
take_offs = np.array([28, 39, 29, 36, 34, 26, 36, 28, 32, 36])
takeof_unc = np.array([2, 3, 3, 3, 3, 3, 3, 3, 3, 4])

# use these for plotting tests only
# polarities = np.array([1, 0])
# back_azimuths = np.array([45, 45])
# az_unc = np.array([5, 6])
# take_offs = np.array([13, 83])

takeoff_spread, takeoff_prob, backazimuth_spread, backazimuth_prob = \
    get_spread_from_takeoff_backaz_unc(takeoff=take_offs, tunc=takeof_unc, backaz=back_azimuths, unc=az_unc, npts=npts)

prob = np.sqrt(backazimuth_prob * takeoff_prob)
prob = prob / np.max(prob)

x, y = find_xy_from_takeoff_backaz(radius=radius, takeoff=take_offs, backaz=back_azimuths)
x_spread, y_spread = find_xy_from_takeoff_backaz(radius=radius, takeoff=takeoff_spread, backaz=backazimuth_spread)
polarities_spread = np.concatenate([np.ones(npts) * p for p in polarities])


# lower_hemisphere_parameters
lower_ss = [strike, dip, rake_ss]
lower_inv = [strike, dip, rake_inv]
lower_oblique = [strike, dip, rake_oblique]

# upper hemisphere parameters
upper_dip = dip
upper_strike = strike+180
upper_ss = [upper_strike, upper_dip, rake_ss]
upper_inv = [upper_strike, upper_dip, rake_inv]
upper_oblique = [upper_strike, upper_dip, rake_oblique]

# make the beachballs
beach_lower_ss = beach(lower_ss, xy=(0, 0), width=2*radius, zorder=0, facecolor=facecolor, bgcolor=bgcolor)
beach_lower_inv = beach(lower_inv, xy=(0, 0), width=2*radius, zorder=0, facecolor=facecolor, bgcolor=bgcolor)
beach_lower_oblique = beach(lower_oblique, xy=(0, 0), width=2*radius, zorder=0, facecolor=facecolor, bgcolor=bgcolor)

beach_upper_ss = beach(upper_ss, xy=(0, 0), width=2*radius, zorder=0, facecolor=facecolor, bgcolor=bgcolor)
beach_upper_inv = beach(upper_inv, xy=(0, 0), width=2*radius, zorder=0, facecolor=facecolor, bgcolor=bgcolor)
beach_upper_oblique = beach(upper_oblique, xy=(0, 0), width=2*radius, zorder=0, facecolor=facecolor, bgcolor=bgcolor)

beach_upper_ss_copy = beach(upper_ss, xy=(0, 0), width=2*radius, zorder=0, facecolor=facecolor, bgcolor=bgcolor)
beach_upper_inv_copy = beach(upper_inv, xy=(0, 0), width=2*radius, zorder=0, facecolor=facecolor, bgcolor=bgcolor)
beach_upper_oblique_copy = beach(upper_oblique, xy=(0, 0), width=2*radius, zorder=0, facecolor=facecolor, bgcolor=bgcolor)


figure, axes = plt.subplots(nrows=3, ncols=3, figsize=(9, 9))
ax = axes[0, 0]
ax.add_collection(beach_lower_ss)
ax.set_aspect("equal")
ax.set_xlim((-radius-10, radius+10))
ax.set_ylim((-radius-10, radius+10))
ax.set_title('Lower hemisphere')
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.text(x=radius-2.5, y=radius-2.5, s='a', weight='bold')

ax = axes[0, 1]
ax.add_collection(beach_lower_inv)
ax.set_aspect("equal")
ax.set_xlim((-radius-10, radius+10))
ax.set_ylim((-radius-10, radius+10))
ax.set_title('Lower hemisphere')
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.text(x=radius-2.5, y=radius-2.5, s='b', weight='bold')

ax = axes[0, 2]
ax.add_collection(beach_lower_oblique)
ax.set_aspect("equal")
ax.set_xlim((-radius-10, radius+10))
ax.set_ylim((-radius-10, radius+10))
ax.set_title('Lower hemisphere')
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.text(x=radius-2.5, y=radius-2.5, s='c', weight='bold')

ax = axes[1, 0]
plt.sca(ax)
ax.add_collection(beach_upper_ss)
ax.set_aspect("equal")
ax.set_xlim((-radius-10, radius+10))
ax.set_ylim((-radius-10, radius+10))
ax.set_title('Upper hemisphere')
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.text(x=radius-2.5, y=radius-2.5, s='d', weight='bold')
ax.scatter(x[polarities == 0], y[polarities == 0],
           marker='+', c='y', s=10, alpha=0.4)
ax.scatter(x[polarities == 1], y[polarities == 1],
           marker='+', c='r', s=10, alpha=0.4)

ax = axes[1, 1]
ax.add_collection(beach_upper_inv)
ax.set_aspect("equal")
ax.set_xlim((-radius-10, radius+10))
ax.set_ylim((-radius-10, radius+10))
ax.set_title('Upper hemisphere')
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.text(x=radius-5, y=radius-5, s='e', weight='bold')
ax.scatter(x[polarities == 0], y[polarities == 0],
           marker='+', c='y', s=10, alpha=0.4)
ax.scatter(x[polarities == 1], y[polarities == 1],
           marker='+', c='r', s=10, alpha=0.4)

ax = axes[1, 2]
ax.add_collection(beach_upper_oblique)
ax.set_aspect("equal")
ax.set_xlim((-radius-10, radius+10))
ax.set_ylim((-radius-10, radius+10))
ax.set_title('Upper hemisphere')
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.text(x=radius-2.5, y=radius-2.5, s='f', weight='bold')
ax.scatter(x[polarities == 0], y[polarities == 0],
           marker='+', c='y', s=10, alpha=0.4)
ax.scatter(x[polarities == 1], y[polarities == 1],
           marker='+', c='r', s=10, alpha=0.4)

x_min_max_spread = max(x_spread) - min(x_spread)
y_min_max_spread = max(y_spread) - min(y_spread)
x_center = (max(x_spread) + min(x_spread)) / 2
y_center = (max(y_spread) + min(y_spread)) / 2
max_spread = max([x_min_max_spread, y_min_max_spread])

xlim1 = x_center - max_spread / 2 - max_spread / 20
xlim2 = x_center + max_spread / 2 + max_spread / 20
ylim1 = y_center - max_spread / 2 - max_spread / 20
ylim2 = y_center + max_spread / 2 + max_spread / 20

ax = axes[2, 0]
plt.sca(ax)
ax.add_collection(beach_upper_ss_copy)
ax.set_aspect("equal")
ax.set_xlim((xlim1, xlim2))
ax.set_ylim((ylim1, ylim2))
ax.set_title('Upper hemisphere zoom')
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.text(x=x_center+max_spread/2-max_spread/20, y=y_center+max_spread/2-max_spread/20, s='g', weight='bold')
ax.scatter(x_spread[polarities_spread == 0], y_spread[polarities_spread == 0],
           marker='^', c='y', s=20*prob[polarities_spread == 0], alpha=0.4)
ax.scatter(x_spread[polarities_spread == 1], y_spread[polarities_spread == 1],
           marker='+', c='r', s=20*prob[polarities_spread == 1], alpha=0.4)

ax = axes[2, 1]
ax.add_collection(beach_upper_inv_copy)
ax.set_aspect("equal")
ax.set_xlim((xlim1, xlim2))
ax.set_ylim((ylim1, ylim2))
ax.set_title('Upper hemisphere zoom')
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.text(x=x_center+max_spread/2-max_spread/20, y=y_center+max_spread/2-max_spread/20, s='h', weight='bold')
ax.scatter(x_spread[polarities_spread == 0], y_spread[polarities_spread == 0],
           marker='^', c='y', s=20*prob[polarities_spread == 0], alpha=0.4)
ax.scatter(x_spread[polarities_spread == 1], y_spread[polarities_spread == 1],
           marker='+', c='r', s=20*prob[polarities_spread == 1], alpha=0.4)

ax = axes[2, 2]
ax.add_collection(beach_upper_oblique_copy)
ax.set_aspect("equal")
ax.set_xlim((xlim1, xlim2))
ax.set_ylim((ylim1, ylim2))
ax.set_title('Upper hemisphere zoom')
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.text(x=x_center+max_spread/2-max_spread/20, y=y_center+max_spread/2-max_spread/20, s='i', weight='bold')
ax.scatter(x_spread[polarities_spread == 0], y_spread[polarities_spread == 0],
           marker='^', c='y', s=20*prob[polarities_spread == 0], alpha=0.4)
ax.scatter(x_spread[polarities_spread == 1], y_spread[polarities_spread == 1],
           marker='+', c='r', s=20*prob[polarities_spread == 1], alpha=0.4)

plt.savefig('figures/focmecs.png', dpi=300)