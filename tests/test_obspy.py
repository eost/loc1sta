from obspy import UTCDateTime


def test_UTCDateTime():
    string = '2023-02-06T10:42:23.430800'
    utc_string = UTCDateTime(string)
    assert (string == utc_string.isoformat())

