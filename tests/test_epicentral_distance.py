import pytest
import numpy as np
import obspy.taup as taup
from obspy.taup.taup_create import build_taup_model
from loc1sta.epicentral_distance import N_DIST_STEPS
from loc1sta.epicentral_distance import get_sp_time_differences_and_angles
from loc1sta.epicentral_distance import get_distances_from_sp_times
from loc1sta.epicentral_distance import get_location_from_sta_dist_km_azimuth


@pytest.mark.skip(reason="This test is too long and works anyway.")
def test_model_iasp():
    model = taup.TauPyModel("iasp91")
    source_depth_km = 0.0
    dist_deg, sp_times = get_sp_time_differences_and_angles(model, 0, 10, source_depth_km)

    assert (len(dist_deg) == N_DIST_STEPS)
    assert (len(sp_times) == N_DIST_STEPS)


@pytest.mark.skip(reason="This test is too long and works anyway.")
def test_model_west_caspian():
    build_taup_model(filename="data/west_caspian_model.tvel")
    model = taup.TauPyModel("west_caspian_model")
    source_depth_km = 0.0
    dist_deg, sp_times = get_sp_time_differences_and_incident_angles(model, 0, 10, source_depth_km)

    assert (len(dist_deg) == N_DIST_STEPS)
    assert (len(sp_times) == N_DIST_STEPS)


def test_interpolation():
    distances = np.linspace(0, 10, N_DIST_STEPS)
    sp_times = 3 * distances  # create some fake sp times - make it linear, so it's easy to check

    sp_measured = np.random.random(20) * 30.0
    check_distances = get_distances_from_sp_times(sp_measured=sp_measured, sp_from_model=sp_times,
                                                  distances_from_model=distances)

    assert np.allclose(check_distances, sp_measured / 3.0)


def test_get_location_north():
    sta_lat = 39.4
    sta_lon = 49.0
    dist = 111.25
    az = 0

    ev_lat, ev_lon = get_location_from_sta_dist_km_azimuth(sta_lat, sta_lon, dist_km=dist, azimuth=az)

    assert ev_lon == pytest.approx(sta_lon, abs=0.05)
    assert ev_lat == pytest.approx(sta_lat + 1, abs=0.05)


def test_get_location_east():
    sta_lat = 39.4
    sta_lon = 49.0
    dist = 111.25
    az = 90

    ev_lat, ev_lon = get_location_from_sta_dist_km_azimuth(sta_lat, sta_lon, dist_km=dist, azimuth=az)

    assert ev_lon > sta_lon
    assert ev_lat == pytest.approx(sta_lat, abs=0.05)


def test_get_location_south():

    sta_lat = 39.4
    sta_lon = 49.0
    dist = 111.25
    az = 180

    ev_lat, ev_lon = get_location_from_sta_dist_km_azimuth(sta_lat, sta_lon, dist_km=dist, azimuth=az)

    assert ev_lat < sta_lat
    assert ev_lon == pytest.approx(sta_lon, abs=0.05)


def test_get_location_west():

    sta_lat = 39.4
    sta_lon = 49.0
    dist = 111.25
    az = 270

    ev_lat, ev_lon = get_location_from_sta_dist_km_azimuth(sta_lat, sta_lon, dist_km=dist, azimuth=az)

    assert ev_lon < sta_lon
    assert ev_lat == pytest.approx(sta_lat, abs=0.05)


def test_get_location_southwest():

    sta_lat = 39.4
    sta_lon = 49.0
    dist = 111.25
    az = 225

    ev_lat, ev_lon = get_location_from_sta_dist_km_azimuth(sta_lat, sta_lon, dist_km=dist, azimuth=az)

    assert ev_lon < sta_lon
    assert ev_lat < ev_lon
