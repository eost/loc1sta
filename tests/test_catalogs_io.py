import pytest
import numpy as np
from loc1sta.catalogs import read_snuffler_event_markers, read_cluster_spreadsheet


def test_read_snuffler():
    picks = read_snuffler_event_markers(filename='data/highHZ_HASN.txt')
    assert len(picks) > 0


@pytest.mark.skip
def test_read_cluster_spreadsheet():
    cdict = read_cluster_spreadsheet(filename='data/HASN_events_single_station.ods')

    assert 8 in cdict.keys()
    df8 = cdict[8]
    assert pytest.approx(np.mean(df8['S-P interval'].values), 0.01) == 2.8

    first_pick_text = '2023-02-06T01:47:52.859500Z'
    first_pick = df8['P-time'][0]

    assert first_pick_text == first_pick

