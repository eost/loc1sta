import numpy as np
import pytest

from loc1sta.waveforms import create_p_wave, create_noisy_P_wave
from loc1sta.polarization_analysis import compute_azimuthal_parameters, compute_azimuthal_parameters_flinn, compute_azimuthal_parameters_pm


def test_p_wave_vertical():
    sigma = 1.0
    azimuth = 45
    plunge = 90

    st_p = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)

    assert np.allclose(st_p.select(channel='HHE').traces[0].data, 0.0)


def test_p_wave_horizontal():
    sigma = 1.0
    azimuth = 45
    plunge = 0

    st_p = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)

    assert np.allclose(st_p.select(channel='HHZ').traces[0].data, 0.0)
    assert np.allclose(st_p.select(channel='HHE').traces[0].data, st_p.select(channel='HHN').traces[0].data)
    assert max(st_p.select(channel='HHE').traces[0].data) > 0


def test_azimuth_0():
    sigma = 1.0
    azimuth = 0.0
    plunge = 10.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = compute_azimuthal_parameters(p_wave)

    assert f_azimuth == pytest.approx(azimuth, abs=f_azimuth_uncertainty)
    assert f_incidence == pytest.approx(90-plunge, abs=f_incidence_uncertainty)


def test_azimuth_0_Roberts_vs_Flinn_incidence():
    sigma = 1.0
    azimuth = 0.0
    plunge = 45.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = \
        compute_azimuthal_parameters(p_wave)

    f_az, f_inc, f_rect, f_plan = compute_azimuthal_parameters_flinn(p_wave)
    assert f_inc == pytest.approx(f_incidence, abs=f_incidence_uncertainty)

def test_azimuth_0_Roberts_vs_PM_incidence():
    sigma = 1.0
    azimuth = 0.0
    plunge = 45.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = \
        compute_azimuthal_parameters(p_wave)

    f_az, f_inc, f_az_err, f_inc_err = compute_azimuthal_parameters_pm(p_wave)
    assert f_inc == pytest.approx(f_incidence, abs=f_inc_err)


def test_azimuth_0_Roberts_vs_Flinn_azimuth():
    sigma = 1.0
    azimuth = 0.0
    plunge = 45.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = \
        compute_azimuthal_parameters(p_wave)

    f_az, f_inc, f_rect, f_plan = compute_azimuthal_parameters_flinn(p_wave)
    assert f_az == pytest.approx(f_azimuth, abs=f_azimuth_uncertainty)

def test_azimuth_0_Roberts_vs_PM_azimuth():
    sigma = 1.0
    azimuth = 0.0
    plunge = 45.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = \
        compute_azimuthal_parameters(p_wave)

    f_az, f_inc, f_az_err, f_inc_err = compute_azimuthal_parameters_pm(p_wave)
    assert f_az == pytest.approx(f_azimuth, abs=f_az_err)

def test_azimuth_90():
    sigma = 1.0
    azimuth = 90.0
    plunge = 65.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = compute_azimuthal_parameters(p_wave)

    assert f_azimuth == pytest.approx(azimuth, abs=0.1)
    assert f_incidence == pytest.approx(90-plunge, abs=0.1)


def test_azimuth_180():
    sigma = 1.0
    azimuth = 180.0
    plunge = 85.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = compute_azimuthal_parameters(p_wave)

    assert f_azimuth == pytest.approx(azimuth, abs=0.1)
    assert f_incidence == pytest.approx(90-plunge, abs=0.1)


def test_azimuth_75():
    sigma = 1.0
    azimuth = 75.0
    plunge = 85.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = compute_azimuthal_parameters(p_wave)

    assert f_incidence == pytest.approx(90-plunge, abs=f_incidence_uncertainty)
    assert f_azimuth == pytest.approx(azimuth, abs=0.1)



def test_azimuth_75_flinn():
    sigma = 1.0
    azimuth = 75.0
    plunge = 85.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_az, f_inc, f_rect, f_plan = compute_azimuthal_parameters_flinn(p_wave)

    assert f_rect == pytest.approx(1, abs=0.01)
    assert f_plan == pytest.approx(1, abs=0.01)
    assert f_inc == pytest.approx(90-plunge, abs=0.01)
    assert f_az == pytest.approx(azimuth, abs=0.01)



def test_azimuth_135_pm():
    sigma = 1.0
    azimuth = 135.0
    plunge = 85.0

    p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
    f_az, f_inc, f_az_err, f_inc_err = compute_azimuthal_parameters_pm(p_wave)

    assert f_az == pytest.approx(azimuth, abs=0.1)
    assert f_inc == pytest.approx(90-plunge, abs=0.1)

def test_azimuth_75_noisy():
        sigma = 1.0
        azimuth = 75.0
        plunge = 85.0
        snr = 2
        horiz = True
        vert = True

        p_wave = create_p_wave(sigma=sigma, azimuth=azimuth, plunge=plunge)
        p_noisy = create_noisy_P_wave(sigma=sigma, azimuth=azimuth, plunge=plunge, noise_amp=1/snr,
                                      horiz=horiz, vert=vert)

        f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = \
            compute_azimuthal_parameters(p_wave)

        assert f_azimuth == pytest.approx(azimuth, abs=0.1)
        assert f_incidence == pytest.approx(90 - plunge, abs=0.1)

        f_incidence_noisy, f_incidence_uncertainty_noisy, f_azimuth_noisy, f_azimuth_uncertainty_noisy, \
            f_coherence_noisy = compute_azimuthal_parameters(p_noisy)

        assert f_azimuth == pytest.approx(f_azimuth_noisy, abs=0.1)
        assert f_incidence == pytest.approx(f_incidence_noisy, abs=0.1)


def test_p_wave_generation():
    from loc1sta.graphics import plot_table_of_streams
    sigma = 1.0
    azimuth = [0, 45, 90, 135, 180, 225, 270, 315]
    plunge = 82.0
    noise_amp = 0.5

    stream_list = [create_p_wave(sigma=sigma, azimuth=az, plunge=plunge) for az in azimuth]
    assert(len(azimuth) == len(stream_list))
    plot_table_of_streams(stream_list, zero_shift=0, filename='figures/test_p_wave.png', xticks=[0, 4, 8, 12])


    stream_list = [create_noisy_P_wave(sigma=sigma, azimuth=az, plunge=plunge, noise_amp=noise_amp,
                              horiz=True, vert=False) for az in azimuth]
    plot_table_of_streams(stream_list, zero_shift=0, filename='figures/test_noisy_p_wave.png', xticks=[0, 4, 8, 12])

    az_noisy = []
    for i in range(len(azimuth)):
        f_incidence, f_incidence_uncertainty, f_azimuth, f_azimuth_uncertainty, f_coherence = compute_azimuthal_parameters(
            stream_list[i])
        az_noisy.append(f_azimuth)

    assert np.allclose(az_noisy, azimuth)

