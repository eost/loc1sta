import pytest
import numpy as np
import ploc.ploc_io as io
import ploc.locate as loc
from obspy.core import Stream, Trace


# ###################
# Configuration tests
# ###################

def get_config():
    return io.Config('test_data/ploc_config.cfg')


def test_config_read():
    config = get_config()
    assert config.nsamples == 1000
    assert config.STA_lat == -49.351
    assert config.STA_lon == 70.2107


# ###################
# Synthetic polarization tests
# ###################

def synthetic_PP(dip, azimuth, dt, npts, T):

    t = np.arange(npts) * dt
    seis = np.sin(2*np.pi * t / T)

    seis_z = seis * np.sin(dip * np.pi / 180.0)
    seis_e = seis * np.cos(dip * np.pi / 180.0) * (-1) * np.sin(azimuth * np.pi / 180.0)
    seis_n = seis * np.cos(dip * np.pi / 180.0) * (-1) * np.cos(azimuth * np.pi / 180.0)

    return t, seis, seis_z, seis_e, seis_n


def test_pol_north():

    t, seis, seis_z, seis_e, seis_n = synthetic_PP(30, 0, 0.1, 400, 4.0)

    assert np.sum(np.abs(seis_e)) < 1e-7
    assert np.abs(seis_n[10] + np.max(seis_n)) < 1e-7


def test_pol_west():

    t, seis, seis_z, seis_e, seis_n = synthetic_PP(30, 270, 0.1, 400, 4.0)

    assert np.sum(np.abs(seis_n)) < 1e-7
    assert np.abs(seis_e[10] - np.max(seis_e)) < 1e-7


def test_pol_south_west():

    t, seis, seis_z, seis_e, seis_n = synthetic_PP(30, 225, 0.1, 400, 4.0)

    assert np.abs(seis_e[10] - seis_n[10]) < 1e-7


def test_pol_all_angles():

    dip = np.arange(9) + 0.5 * 10.0
    az = np.arange(36) + 0.5 * 10.0
    for d in dip:
        for a in az:
            t, seis, seis_z, seis_e, seis_n = synthetic_PP(d, a, 0.1, 400, 4.0)
            tr_z = Trace(seis_z, header={'delta' : 0.1, 'channel' : 'BHZ'})
            tr_e = Trace(seis_e, header={'delta' : 0.1, 'channel' : 'BHE'})
            tr_n = Trace(seis_n, header={'delta' : 0.1, 'channel' : 'BHN'})
            st = Stream([tr_z, tr_e, tr_n])
            rectilinP, azimuthP, dipP, Plani = loc.get_polarization_stuff(st, 0, 40)

            # print d, np.abs(dipP)
            # print a, azimuthP
            assert np.abs(d - np.abs(dipP)) < 1e-7
            assert np.abs(a - (90 -azimuthP)) < 1e-7


def test_dist_az_to_lat_lon_North():

    cfg = get_config()
    cfg.STA_lat = 0.0
    cfg.STA_lon = 0.0

    dist = 111.25
    az = 90. 
    lat, lon = loc.dist_az_to_lat_lon(dist, az, cfg)

    assert np.abs(lat - 1.0) < 1e-7
    assert np.abs(lon - 0.0) < 1e-7

def test_dist_az_to_lat_lon_East():

    cfg = get_config()
    cfg.STA_lat = 0.0
    cfg.STA_lon = 0.0

    dist = 111.25
    az = 0. 
    lat, lon = loc.dist_az_to_lat_lon(dist, az, cfg)

    assert np.abs(lat - 0.0) < 1e-7
    assert np.abs(lon - 1.0) < 1e-7

def test_dist_az_to_lat_lon_West():

    cfg = get_config()
    cfg.STA_lat = 0.0
    cfg.STA_lon = 0.0

    dist = 111.25
    az = 180. 
    lat, lon = loc.dist_az_to_lat_lon(dist, az, cfg)

    assert np.abs(lat - 0.0) < 1e-7
    assert np.abs(lon + 1.0) < 1e-7

def test_dist_az_to_lat_lon_South():

    cfg = get_config()
    cfg.STA_lat = 0.0
    cfg.STA_lon = 0.0

    dist = 111.25
    az = -90. 
    lat, lon = loc.dist_az_to_lat_lon(dist, az, cfg)

    assert np.abs(lat + 1.0) < 1e-7
    assert np.abs(lon - 0.0) < 1e-7


