import os
import matplotlib.pyplot as plt
import numpy as np
import locate as loc
from mpl_toolkits.basemap import Basemap
from scipy.stats import norm


def plot_events(events, cfg, fname):


    npts = len(events)

    c_lat = cfg.STA_lat
    c_lon = cfg.STA_lon
    lat_0 = np.mean((cfg.llcrnrlat, cfg.urcrnrlat))
    lon_0 = np.mean((cfg.llcrnrlon, cfg.urcrnrlon))
    map = Basemap(llcrnrlon=cfg.llcrnrlon, llcrnrlat=cfg.llcrnrlat,
                  urcrnrlon=cfg.urcrnrlon, urcrnrlat=cfg.urcrnrlat,
                  resolution='h', projection='tmerc', lat_0=lat_0, lon_0=lon_0)
    map.drawcoastlines()

    x_STA, y_STA = map(cfg.STA_lon, cfg.STA_lat)

    if cfg.plot_scatter:
        # start by adding scatter points (calculate on the fly)
        for i in xrange(npts):
            d = norm.rvs(events[i]['Distkm'], events[i]['Distkm_sigma'], cfg.nsamples)
            a = norm.rvs(events[i]['AzimP'], events[i]['AzimP_sigma'], cfg.nsamples)
            lat, lon = loc.dist_az_to_lat_lon(d, a, cfg)
            x, y = map(lon, lat)
            map.scatter(x, y, marker='.', color='c')
        

    lat = np.empty(npts, float)
    lon = np.empty(npts, float)
    for i in xrange(npts):
        lat[i] = events[i]['Lat']
        lon[i] = events[i]['Lon']
    x, y = map(lon, lat)
    map.scatter(x, y, marker='D', color='m')
    map.plot(x_STA, y_STA, marker='s', color='b')

    plt.savefig(os.path.join(cfg.fig_dir, fname))

def plot_hodochrones(d, ttime_list, names, cfg, fname):

    n_hod = len(ttime_list)

    fig = plt.figure()
    for i in xrange(n_hod):
        plt.plot(d, ttime_list[i], label=names[i])

    plt.xlabel('Distance (km)')
    plt.ylabel('Arrival time (s)')

    plt.legend(loc='lower right')

    plt.savefig(os.path.join(cfg.fig_dir, fname))
    plt.close(fig)
