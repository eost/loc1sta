import ConfigParser
import os


class Config(object):

    def __init__(self, fname):
        config = ConfigParser.ConfigParser()
        config.read(fname)

        # Bootstrap
        self.nsamples = config.getint('Bootstrap', 'nsamples')

        # Station
        self.STA_lat = config.getfloat('Station', 'STA_lat')
        self.STA_lon = config.getfloat('Station', 'STA_lon')

        # Data
        self.data_dir = config.get('Data', 'data_dir')
        self.data_files = self.parse_list_(config.get('Data', 'data_files'))
        self.sigma_tp_def = config.getfloat('Data', 'sigma_tp_def')
        self.sigma_ts_def = config.getfloat('Data', 'sigma_ts_def')

        self.do_filter = config.getboolean('Data', 'do_filter')
        if self.do_filter:
            self.filter_type = config.get('Data', 'filter_type')
            self.freq = config.getfloat('Data', 'freq')

        # Pol
        self.wlen0 = config.getfloat('Pol', 'wlen0')
        self.sigma_wlen = config.getfloat('Pol', 'sigma_wlen')
        self.RectP_limit = config.getfloat('Pol', 'RectP_limit')

        # Earth_Model
        self.vmodel = config.get('Earth_Model', 'vmodel')
        if self.vmodel == 'hom':
            self.Vp0 = config.getfloat('Earth_Model', 'Vp0')
            self.nu0 = config.getfloat('Earth_Model', 'nu0')
            self.sigma_Vp = config.getfloat('Earth_Model', 'sigma_Vp')
            self.sigma_nu = config.getfloat('Earth_Model', 'sigma_nu')
        elif self.vmodel == '2layer':
            self.dist_max = config.getfloat('Earth_Model', 'dist_max')
            self.h1 = config.getfloat('Earth_Model', 'h1')
            self.h2 = config.getfloat('Earth_Model', 'h2')
            self.Vp1 = config.getfloat('Earth_Model', 'Vp1')
            self.Vp2 = config.getfloat('Earth_Model', 'Vp2')
            self.Vp3 = config.getfloat('Earth_Model', 'Vp3')
            self.nu0 = config.getfloat('Earth_Model', 'nu0')
            self.sigma_h1 = config.getfloat('Earth_Model', 'sigma_h1')
            self.sigma_h2 = config.getfloat('Earth_Model', 'sigma_h2')
            self.sigma_Vp1 = config.getfloat('Earth_Model', 'sigma_Vp1')
            self.sigma_Vp2 = config.getfloat('Earth_Model', 'sigma_Vp2')
            self.sigma_Vp3 = config.getfloat('Earth_Model', 'sigma_Vp3')
            self.sigma_nu = config.getfloat('Earth_Model', 'sigma_nu')

        # Verbosity
        self.verbose = config.getboolean('Verbosity', 'verbose')

        # Plotting
        self.do_plots = config.getboolean('Plotting', 'do_plots')
        if self.do_plots:
            self.fig_dir = config.get('Plotting', 'fig_dir')
            if not os.path.isdir(self.fig_dir):
                os.mkdir(self.fig_dir)
            self.llcrnrlon = config.getfloat('Plotting', 'llcrnrlon')
            self.llcrnrlat = config.getfloat('Plotting', 'llcrnrlat')
            self.urcrnrlon = config.getfloat('Plotting', 'urcrnrlon')
            self.urcrnrlat = config.getfloat('Plotting', 'urcrnrlat')
            self.plot_scatter = config.getboolean('Plotting', 'plot_scatter')

    def parse_list_(self, list_as_string):

        words = list_as_string.split(',')
        ret_list = []
        for w in words:
            ret_list.append(w.strip())
        return ret_list

        
def read_detected_event(hdr_filename, station, delta, tosac=False):
    # reads data in Olivier format

    basename =  hdr_filename.split('.hdr')[0]
    dat_filename = '%s.dat' % basename

    with open(hdr_filename, 'r') as f_:

        first_line = f_.readline()
        second_line = f_.readline()
        words = first_line[18:-1].split()
        lat = float(words[0])
        lon = float(words[1])
        elev = float(words[2])

        year = int(first_line[0:4])
        month = int(first_line[4:6])
        day = int(first_line[6:8])
        hour = int(first_line[8:10])
        minute = int(first_line[10:12])
        second = float(first_line[12:17])

        starttime = obspy.UTCDateTime(year, month, day, hour, minute, second) 
        iclust = int(second_line.split()[0])

    d = np.fromfile(dat_filename, dtype='int32')
    tr_Z = obspy.Trace(d[0:512], header={'delta' : delta, 'starttime' : starttime, 'station' : 'PAF', 'channel' : 'BHZ', 'network' : 'G', 'sac' : {'nevid' : iclust}})
    tr_N = obspy.Trace(d[512:1024], header={'delta' : delta, 'starttime' : starttime, 'station' : 'PAF', 'channel' : 'BHN', 'network' : 'G', 'sac' : {'nevid' : iclust}})
    tr_E = obspy.Trace(d[1024:1536], header={'delta' : delta, 'starttime' : starttime, 'station' : 'PAF', 'channel' : 'BHE', 'network' : 'G', 'sac' : {'nevid' : iclust}})

    st = obspy.Stream([tr_Z, tr_N, tr_E])

    if tosac and len(tr_Z.data) > 0:
        for tr in st:
            tr.write("%s_%s.sac" % (basename, tr.stats.channel), format='SAC')

    return st

