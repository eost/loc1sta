import numpy as np
from scipy.stats import norm
from scipy.interpolate import interp1d


def hom_get_circle_dist(Vp, Vs, tstp):

    return ((Vp * Vs) / (Vp - Vs)) * tstp


def hodochrone_2layers(d, h1, h2, c1, c2, c3, only_first_arrival=False):

    # direct waves in upper layer
    tpd = d / c1

    # waves refracted under 1st layer
    c21 = c2 / c1
    l1p = h1 * c21
    d1p = h1 * np.sqrt(c21**2 - 1)
    tpg = 2 * l1p / c1 + (d - 2 * d1p) / c2

    # waves refracted under 2nd layer
    c32 = c3 / c2
    c1322 = c1 + c2
    l2p = h2 * c32
    d2p = h2 * np.sqrt(c32**2 - 1)
    l1p = h1 * c1322
    d1p2 = h1 * np.sqrt(c1322**2 - 1)
    tpn = 2 * l1p / c1 + 2 * l2p / c2 + (d - 2*d1p2 - 2*d2p) / c3

    nlen = len(d)
    # add NaNs where required
    for i in xrange(nlen):
        # no 1st layer refraction before total reflection
        if d[i] < 2 * d1p:
            tpg[i] = np.nan
        if d[i] < 2 * (d1p2 + d2p):
            tpn[i] = np.nan

    first_arrival = np.nanmin((tpd, tpg, tpn), axis=0)

    if only_first_arrival:
        return first_arrival
    else:
        return tpd, tpg, tpn, first_arrival


def get_dist_from_tstp(d, hod_tstp, tstp):

    # interpolate
    f = interp1d(hod_tstp, d)
    dist = f(tstp)

    return dist


def hom_get_circle_dist_distrib(picks, sigma_picks, cfg):

    # get picks and their uncertainties
    tp0, ts0 = picks
    sigma_tp, sigma_ts = sigma_picks

    Vp = norm.rvs(cfg.Vp0, cfg.sigma_Vp, cfg.nsamples)
    nu = norm.rvs(cfg.nu0, cfg.sigma_nu, cfg.nsamples)
    Vs = Vp / nu

    tp = norm.rvs(tp0, sigma_tp, cfg.nsamples)
    ts = norm.rvs(ts0, sigma_ts, cfg.nsamples)
    tstp = ts-tp

    dist = hom_get_circle_dist(Vp, Vs, tstp)
    return dist


def layer2_get_circle_dist_distrib(picks, sigma_picks, cfg):

    # get picks and their uncertainties
    tp0, ts0 = picks
    sigma_tp, sigma_ts = sigma_picks

    # get model parameters
    h1 = norm.rvs(cfg.h1, cfg.sigma_h1, cfg.nsamples)
    h2 = norm.rvs(cfg.h2, cfg.sigma_h2, cfg.nsamples)
    Vp1 = norm.rvs(cfg.Vp1, cfg.sigma_Vp1, cfg.nsamples)
    Vp2 = norm.rvs(cfg.Vp2, cfg.sigma_Vp2, cfg.nsamples)
    Vp3 = norm.rvs(cfg.Vp3, cfg.sigma_Vp3, cfg.nsamples)
    nu = norm.rvs(cfg.nu0, cfg.sigma_nu, cfg.nsamples)
    Vs1 = Vp1 / nu
    Vs2 = Vp2 / nu
    Vs3 = Vp3 / nu

    tp = norm.rvs(tp0, sigma_tp, cfg.nsamples)
    ts = norm.rvs(ts0, sigma_ts, cfg.nsamples)
    tstp = ts-tp

    d = np.arange(cfg.dist_max)

    # get distance distribution (bootstrap)
    dist = np.empty(cfg.nsamples, dtype=float)
    for i in xrange(cfg.nsamples):
        fa_p = hodochrone_2layers(d, h1[i], h2[i], Vp1[i], Vp2[i], Vp3[i], only_first_arrival=True)
        fa_s = hodochrone_2layers(d, h1[i], h2[i], Vs1[i], Vs2[i], Vs3[i], only_first_arrival=True)
        hod_tstp = fa_s - fa_p
        dist[i] = get_dist_from_tstp(d, hod_tstp, tstp[i])
    return dist

def get_polarization_stuff(st, tp, wlen):

    Ztrace = st.select(component="Z")[0]
    Ntrace = st.select(component="N")[0]
    Etrace = st.select(component="E")[0]

    # normalise to get decent numbers
    istart = int(tp / Ztrace.stats.delta)
    iend = int((tp + wlen) / Ztrace.stats.delta)
    maxZ = max(abs(Ztrace.data[istart:iend]))
    xP = Etrace.data[istart:iend] / maxZ
    yP = Ntrace.data[istart:iend] / maxZ
    zP = Ztrace.data[istart:iend] / maxZ

    try:
        MP = np.cov(np.array([xP, yP, zP]))
        w, v = np.linalg.eig(MP)
    except np.linalg.linalg.LinAlgError:
        return np.NaN, np.NaN, np.NaN, np.NaN

    indexes = np.argsort(w)
    DP = w[indexes]
    pP = v[:, indexes]

    rectilinP = 1 - ((DP[0] + DP[1]) / (2*DP[2]))
    azimuthP = np.arctan(pP[1, 2] / pP[0, 2]) * 180./np.pi
    dipP = np.arctan(pP[2, 2] / np.sqrt(pP[1, 2]**2 + pP[0, 2]**2)) * 180/np.pi
    Plani = 1 - (2 * DP[0]) / (DP[1] + DP[2])

    return rectilinP, azimuthP, dipP, Plani


def get_polarization_distrib(st, tp0, sigma_tp, cfg):

    tp = norm.rvs(tp0, sigma_tp, cfg.nsamples)
    wlen = norm.rvs(cfg.wlen0, cfg.sigma_wlen, cfg.nsamples)

    polarization = np.empty((cfg.nsamples, 4), dtype=float)
    for i in xrange(cfg.nsamples):
        rectilinP, azimuthP, dipP, Plani = get_polarization_stuff(st, tp[i], wlen[i])
        polarization[i, 0] = rectilinP
        polarization[i, 1] = azimuthP
        polarization[i, 2] = dipP
        polarization[i, 3] = Plani

    return polarization


def get_dist_pol_from_st(st, cfg):

    Ztrace = st.select(component="Z")[0]
    tp0 = Ztrace.stats.sac['t0']
    ts0 = Ztrace.stats.sac['t1']
    picks = (tp0, ts0)
    # TODO
    # for now just put the default sigmas here. Needs to change sometime.
    sigma_picks = (cfg.sigma_tp_def, cfg.sigma_ts_def)
    
    # get distance (depends on model)
    if cfg.vmodel == 'hom':                                            
        dist = hom_get_circle_dist_distrib(picks, sigma_picks, cfg)
    elif cfg.vmodel == '2layer':
        dist = layer2_get_circle_dist_distrib(picks, sigma_picks, cfg)
    else:
        raise UserWarning('Unknown vmodel')

    # get polarizations (model independent)
    polarization = get_polarization_distrib(st, tp0, sigma_picks[0], cfg)
    means = np.mean(polarization, axis=0)
    stds = np.std(polarization, axis=0)

    dist_pol = {'Distkm' : np.mean(dist), 
                'Distkm_sigma' : np.std(dist),
                'RectP' : means[0],
                'RectP_sigma' : stds[0],
                'AzimP' : means[1],
                'AzimP_sigma' : stds[1],
                'DipP' : means[2],
                'DipP_sigma' : stds[2],
                'PlanP' : means[3],
                'PlanP_sigma' : stds[3]
               }

    if cfg.verbose:
        print st
        print "Distance = (%.2f pm %.2f) km" % (np.mean(dist), np.std(dist))
        print "Rectilinarity P = %.2f pm %.2f" % (means[0], stds[0])
        print "Azimuth P = (%.2f pm %.2f) deg" % (means[1], stds[1])
        print "Dip P = (%.2f pm %.2f) deg" % (means[2], stds[2])
        print "Planarity = %.2f pm %.2f" % (means[3], stds[3])

    return dist_pol


def get_lat_lon_from_dist_pol(dist_pol, cfg):
    
    dist = dist_pol['Distkm']
    # turn the degrees (from polarization) to radians
    az = dist_pol['AzimP']
    
    # get lat lon of central point 
    dist_pol['Lat'], dist_pol['Lon'] = dist_az_to_lat_lon(dist, az, cfg)
    if cfg.verbose:
        print "Location = (%.2f, %.2f)" % (dist_pol['Lat'], dist_pol['Lon'])


def dist_az_to_lat_lon(dist, az, cfg):

    # Azimuth is in geometrical coordinate (positive up from x axis) and in degrees
    Lat = cfg.STA_lat + dist * np.sin(az * np.pi / 180.0 ) / 111.25
    Lon = cfg.STA_lon + dist * np.cos(az * np.pi / 180.0) / 111.25 * \
                        np.cos(cfg.STA_lat * np.pi / 180.0)

    return Lat, Lon
