import os
import argparse
from obspy.core import read
import ploc.ploc_io as io
import ploc.locate as loc
import ploc.graphics as gr

def run_locate(cfg):
    # do location
    # do this with a list of dictionnaries for now
    # TODO : migrate to pandas
    events = []
    for f in cfg.data_files:
        st = read(os.path.join(cfg.data_dir, f))
        if cfg.do_filter:
            st.filter(cfg.filter_type, freq=cfg.freq)
        dist_pol = loc.get_dist_pol_from_st(st, cfg)
        loc.get_lat_lon_from_dist_pol(dist_pol, cfg)
        dist_pol['Filename'] = f
        # keep event only if rectininearity of P-wave is above 90%
        if dist_pol['RectP'] > cfg.RectP_limit:
            events.append(dist_pol)
    return events
    

def run_plotting(events, cfg):
    if cfg.plot_scatter:
        gr.plot_events(events, cfg, 'ploc_events_scatter.png')
    else:
        gr.plot_events(events, cfg, 'ploc_events.png')


def run_all(args):
    cfg = io.Config(args.config_file)
    events = run_locate(cfg)
    if cfg.do_plots:
        run_plotting(events, cfg)


if __name__ == '__main__':

   # set up parser
    parser = argparse.ArgumentParser(
        description='Launch single station location code')
    parser.add_argument('config_file', help='ploc configuration file')

    # parse input
    args = parser.parse_args()

    # run program
    run_all(args)
