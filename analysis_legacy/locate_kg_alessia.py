import numpy as np
from obspy.core import UTCDateTime
from obspy.clients.fdsn import Client
from obspy.taup import TauPyModel
from obspy.taup.taup_create import build_taup_model
from scipy import interpolate
import magnitude
from pyproj import Proj
import pandas as pd
import geopandas as gpd
import contextily as cx
from matplotlib_scalebar.scalebar import ScaleBar
import matplotlib.pyplot as plt


# The purpose of this program is to locate events around Kerguelen island using
# only one station. We compute distance and azimuth to source
# Location follows Roberts et al. GJI 1989

# Build Ts-Tp travel time database (once for all)
build_taup_model('/Users/olivier/Travail/Kerguelen/kerguelen.nd')
model = TauPyModel(model='kerguelen')
depths = np.arange(4,14); depths = depths+0.9 # To avoid being actually at the transition
dists = np.arange(0,160); dists = dists/111.1 # Convert to degrees
DTSP = np.zeros((len(depths),len(dists)))
for idepth, depth in enumerate(depths):
    print(idepth)
    for idist, dist in enumerate(dists):
        arrivals = model.get_travel_times(source_depth_in_km=depth,distance_in_degree=dist,phase_list=['p','P','Pn'])
        flag_p = 0; flag_s = 0
        for arrival in arrivals:
            # This is the first P arrival
            if((arrival.name[0] == 'P') or (arrival.name[0] == 'p') and (flag_p == 0)) :
                tp  = arrival.time
                flag_p = 1
            if((arrival.name[0] == 'S') or (arrival.name[0] == 's') and (flag_s == 0)) :
                ts  = arrival.time
                flag_s = 1    
                
        DTSP[idepth,idist] = ts-tp


# Get an integrate estimate over all possible depth range
dtsp = np.mean(DTSP,axis=0)
f = interpolate.interp1d(dtsp, dists,bounds_error=False,fill_value=(0.009,-9)) # Get the function to return the distance (degrees) from the ts -tp

plot=False
E = {}
evt=-1

# Set the length of the P-wave puls in sample
ipb = 3  # Number of samples before pick
ipa = 15 # Number of samples after pick

# Set number of samples to keep in the trace
ipbb = 50  # Number of samples before pick
ipaa = 250 # Number of samples after pick

# Initialize arrays where we will keep records
AZ = [] ;  D= [];  P=[]; U=[]; CH=[]; AZ_U=[]; T= []; M=[]

# Set the client (IPGP has more data than RESIF but some data are stil
# not available online and should be asked to Dimitri
client = Client('IPGP')
tmax = UTCDateTime(2015,1,1)
t1 = UTCDateTime(2008,1,1)

while t1 < tmax :

    # Specifiy time of the day to process
    t2 = t1 + 24*3600 # Endtime is just one day long

    XP = []
    try : 
        # Get the filename with the picks and open the files
        pick_filename_P = 'Picks/{0:04d}/{1:03d}//KG.PAF.P.txt'.format(t1.year,t1.julday)
        pick_filename_S = 'Picks/{0:04d}/{1:03d}//KG.PAF.S.txt'.format(t1.year,t1.julday)
        XP = np.loadtxt(pick_filename_P)
        XS = np.loadtxt(pick_filename_S)
    except :
        print('No data for that day')
        

    # IF there is at least one pick
    if(len(XP)>0):
        # Make the waveform request for that day
        st = client.get_waveforms(network='G',station='PAF',channel='BH*',location='00',starttime=t1-120,endtime=t2+120)
        # Get original waveform
        st0 = st.copy()
        # Apply a high-pass filter
        st.filter("highpass", freq=2.0,zerophase=True)
        
        # Initialize number of valid data
        k=0
        if(len(np.shape(XP)) ==1):
            XP = [XP]
        if(len(np.shape(XS))==1) :
            XS = [XS]      
        # Loop on all P picks
        for xp in XP:
            tp = xp[0] # Get the arrival time
            pp = xp[1] # Get the pobability
            up = xp[2] # Get the uncertainty
            # Now loop on all S picks
            for xs in XS:
                ts = xs[0] # Get the arrival time
                ps = xs[1] # Get the probability
                us = xs[2] # Get the uncertainty

                # Look for a a valid P & S pair (Ts-Tp < 20 seconds)
                if((ts-tp < 20) and (ts-tp >0) and (len(st) == 3)):

                    evt = evt+1
                    
                    # Extract the signal on vertical component around P-wave pulse / complete waveform
                    ip = int(tp*20) - int((st[2].stats.starttime - t1)*20)
                    ip1 = ip-ipb; ip2 = ip+ipa
                    yz = st[2].data[ip1:ip2] # On the vertical component

                    ip1 = ip-ipbb; ip2 = ip+ipaa
                    yzl = st0[2].data[ip1:ip2] # On the vertical component
                                        
                    ip = int(tp*20) - int((st[1].stats.starttime - t1)*20)
                    ip1 = ip-ipb; ip2 =ip+ipa # On the north component
                    yn = st[1].data[ip1:ip2]

                    ip1 = ip-ipbb; ip2 = ip+ipaa
                    ynl = st0[1].data[ip1:ip2] #
                
                    ip = int(tp*20) - int((st[0].stats.starttime - t1)*20)
                    ip1 = ip-ipb; ip2 =ip+ipa
                    ye = st[0].data[ip1:ip2]  # On the east component

                    ip1 = ip-ipbb; ip2 = ip+ipaa
                    yel = st0[0].data[ip1:ip2] #

                    # Compute azimuth 
                    az = np.arctan2(-np.mean(ye*yz),-np.mean(yn*yz))*180/np.pi
                    if(az < 0):
                        az = az+360

                    # Compute Coherence
                    R = np.mean(yz*yz)/(np.sqrt((np.mean(yn*yz))**2+(np.mean(ye*yz))**2))
                    A  = -R*np.cos(az*np.pi/180)
                    B  = -R*np.sin(az*np.pi/180)            
                    C = 1- (np.mean((yz-A*yn-B*ye)**2))/np.mean((yz*yz))
                    az_u = np.mean(yz*yz)*( (np.mean(yn*yz))**2 * np.mean(ye*ye) + (np.mean(ye*yz))**2 * np.mean(yn*yn) - np.mean(yn*yn)*np.mean(ye*yz)*np.mean(yn*ye)) / (((np.mean(yn*yz))**2 + (np.mean(ye*yz))**2)**2 * len(yz)) * 180/np.pi
                
                    dist = f(ts-tp) * 111.1
                    tts = t1+ts
                    ttp = t1+tp
                    mag = magnitude.compute_local_magnitude(st0.copy(),dist,tts,'PAF.xml')
                    
                    # Show the trace
                    if(plot==True):
                        ip = int(tp*20) - int((st[2].stats.starttime - t1)*20)
                        ip1 = ip-100; ip2= ip+2000; 
                        trace = st[2].data[ip1:ip2]
                        plt.plot(trace/np.amax(trace) + k)
                        k=k+1
                        
                    # Store this valid record
                    AZ.append(az)
                    D.append(dist)
                    P.append(pp*ps)
                    U.append(np.sqrt(us**2+up**2))
                    CH.append(C)
                    AZ_U.append(az_u)
                    T.append(ttp.timestamp)
                    M.append(mag)

                    print(ts, tp, dist)
                    E[evt] = {'azimuth' : az, 'distance' : dist,  'proba' : pp*ps,
                              'unc_d' : np.sqrt(us**2+up**2), 'coher' : C, 'unc_az' : az_u,
                              'time' : ttp.timestamp, 'magnitude' : mag, 'se' : yel, 'sn' : ynl,
                              'sz' : yzl, 'ts' : ts, 'tp' : tp}
                    
                    
                    # If we find an S-pick valid - stop 
                    break

    print(t1)
    t1 = t1+24*3600
                
AZ = np.array(AZ)        
D = np.array(D)
P = np.array(P)
U = np.array(U)
CH = np.array(CH)
AZ_U = np.array(AZ_U)
T = np.array(T)
M=np.array(M)

I = np.where( (AZ_U <5) & (CH > -5) & (D > 0))

PAF_lat = -49.351
PAF_lon = 70.210708

p=Proj(proj='utm',zone='42',ellps='WGS84',preserve_units=False)

x0,y0=p(PAF_lon,PAF_lat)

x = D[I] * 1000 * np.cos( (AZ[I]-90)*np.pi/180) + x0
y = D[I] * 1000 * np.sin( (AZ[I]-90)*np.pi/180) + y0

eq_lon, eq_lat = p(x,y,inverse=True)
