import os
import argparse
import tempfile
import urllib
import ConfigParser
import time

from obspy.core import UTCDateTime, read

class Config(object):

    def __init__(self, fname):

        config = ConfigParser.ConfigParser()
        config.read(fname)

        self.query = config.get('FDSN', 'query')

        self.network = config.get('Station', 'network')
        self.staname = config.get('Station', 'staname')
        self.locid = config.get('Station', 'locid')
        self.channel = config.get('Station', 'channel')
        self.STA_lat = config.getfloat('Station', 'STA_lat')
        self.STA_lon = config.getfloat('Station', 'STA_lon')


        self.time_type = config.get('Times', 'time_type')
        self.year = config.getint('Times', 'year')
        if self.time_type == 'single_day':
            self.day = config.getint('Times', 'day')
        elif self.time_type == 'multiple_days':
            self.start_day = config.getint('Times', 'start_day')
            self.end_day = config.getint('Times', 'end_day')
        elif self.time_type == 'specific_times':
            self.duration = config.getfloat('Times', 'duration')
            self.start_times = self.parse_list_(config.get('Times', 'start_times'))

        self.data_dir = config.get('Directory', 'data_dir')
        self.verbose = config.getboolean('Verbosity', 'verbose')
        self.verbose_timing = config.getboolean('Verbosity', 'verbose_timing')

    def parse_list_(self, list_as_string):
        words = list_as_string.split(',')
        ret_list = []
        for w in words:
            ret_list.append(w.strip())
        return ret_list


def get_webservice_data(cfg, starttime, endtime):
    tic = time.time()
    url = cfg.query
    url = url + 'network=%s' % cfg.network
    url = url + '&station=%s' % cfg.staname
    url = url + '&location=%s' % cfg.locid
    url = url + '&channel=%s' % cfg.channel
    url = url + '&starttime=%s' % starttime
    url = url + '&endtime=%s' % endtime
    url = url + '&nodata=404'

    f_, fname = tempfile.mkstemp()
    os.close(f_)
    if cfg.verbose:
        print "Retreiving data into file %s ..." % fname
    urllib.urlretrieve(url, fname)

    toc = time.time()

    if cfg.verbose and cfg.verbose_timing:
        print "Time taken for retrieving data: %.2f seconds" % (toc-tic)
    
    return fname


def get_times_for_year_jday(year, jday):

    try:
        starttime = UTCDateTime("%4d-%03d" % (year, jday))
    except ValueError:
        raise UserWarning("%4d-%03d is not a valid date" % (year, jday))

    try:
        endtime = UTCDateTime("%4d-%03d" % (year, jday+1))
    except ValueError:
        endtime = UTCDateTime("%4d-%03d" % (year+1, 1))

    return starttime, endtime

def write_data_as_sacfile(cfg, fname, jday=None):

    try:
        st = read(fname)
        st.detrend(type='linear')
        st.merge(fill_value=0.0)
        for tr in st:
            if jday is not None:
                # trim the data
                stime, etime = get_times_for_year_jday(cfg.year, jday)
                tr.trim(stime, etime, pad=True, fill_value=0.0)
                # use a specific day format
                sacname = os.path.join(cfg.data_dir, '%s.%4d.%03d.SAC' %
                          (tr.id, cfg.year, jday))
            else:
                sacname = os.path.join(cfg.data_dir, '%s_%s.SAC' %
                          (tr.id, tr.stats.starttime.isoformat()))
            tr.write(sacname, format='SAC')
            # nasty re-read to put station lat and lon into file
            tr_tmp = read(sacname)[0]
            tr_tmp.stats.sac['stla'] = cfg.STA_lat
            tr_tmp.stats.sac['stlo'] = cfg.STA_lon
            tr_tmp.write(sacname, format='SAC')
        if cfg.verbose:
            print(st)
    except TypeError:
        print "No data !!\n"
    
    if cfg.verbose:
        print "Removing temporary file %s ..." % fname
    os.unlink(fname)

def get_and_write_single_day(cfg, year, day):
    s_time, e_time = get_times_for_year_jday(year, day)
    fname = get_webservice_data(cfg, s_time, e_time)
    write_data_as_sacfile(cfg, fname, jday=day)

def get_and_write_specific_time(cfg, starttime):
    s_time = UTCDateTime(starttime)
    e_time = s_time + cfg.duration
    fname = get_webservice_data(cfg, s_time.isoformat(), e_time.isoformat())
    write_data_as_sacfile(cfg, fname)

def get_and_write_data(cfg):

    # create output directory if needed
    if not os.path.isdir(cfg.data_dir):
        os.mkdir(cfg.data_dir)

    # select course of action depending on type
    if cfg.time_type == 'single_day':
        get_and_write_single_day(cfg, cfg.year, cfg.day)
    elif cfg.time_type == 'multiple_days':
        for day in xrange(cfg.start_day, cfg.end_day+1):
            get_and_write_single_day(cfg, cfg.year, day)
    elif cfg.time_type == 'specific_times':
        for stime in cfg.start_times:
            get_and_write_specific_time(cfg, stime)
    else:
        print cfg.time_type
        raise NotImplemented


def run_all(args):

      cfg = Config(args.config_file)
      get_and_write_data(cfg)

if __name__ == '__main__':


   # set up parser
    parser = argparse.ArgumentParser(
        description='Launch data retrieval code')
    parser.add_argument('config_file', help='configuration file')

    # parse input
    args = parser.parse_args()

    # run program
    run_all(args)
