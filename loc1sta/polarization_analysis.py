import numpy as np
from scipy.stats import norm
from obspy.signal import polarization as pol


N_SAMPLES = 1000


def compute_incidence_azimuth_uncertainty_coherence(tr_z, tr_n, tr_e):
    # Remember that the azimuth refers to the azimuth of the event with respect to the station.
    # The azimuth of the station with respect to the event is the back-azimuth.

    npts = len(tr_z)

    azimuth = np.arctan2(-np.mean(tr_e * tr_z), -np.mean(tr_n * tr_z)) * 180 / np.pi
    if azimuth < 0:
        azimuth = azimuth + 360

    azimuth_uncertainty = np.mean(tr_z * tr_z) * ((np.mean(tr_n * tr_z)) ** 2 * np.mean(tr_e * tr_e) +
                                                  (np.mean(tr_e * tr_z)) ** 2 * np.mean(tr_n * tr_n) -
                                                  np.mean(tr_n * tr_z) * np.mean(tr_e * tr_z) * np.mean(tr_n * tr_e)) / \
                          (((np.mean(tr_n * tr_z)) ** 2 + (np.mean(tr_e * tr_z)) ** 2) ** 2 * npts) * 180 / np.pi

    r = np.mean(tr_z * tr_z) / (np.sqrt((np.mean(tr_n * tr_z)) ** 2 + (np.mean(tr_e * tr_z)) ** 2))
    a = -r * np.cos(azimuth * np.pi / 180)
    b = -r * np.sin(azimuth * np.pi / 180)
    coherence = 1 - (np.mean((tr_z - a * tr_n - b * tr_e) ** 2)) / np.mean((tr_z * tr_z))

    incidence = np.arctan2(np.sqrt((np.mean(tr_n * tr_z)) ** 2 + (np.mean(tr_e * tr_z)) ** 2), np.mean(tr_z * tr_z)) * \
                180.0 / np.pi

    # Compute uncertainties using monte-carlo (because I'm not sure about the math)
    var_tr_n_tr_z = (np.mean(tr_n * tr_n) * np.mean(tr_z * tr_z) + np.mean(tr_n * tr_z) ** 2) / npts
    var_tr_e_tr_z = (np.mean(tr_e * tr_e) * np.mean(tr_z * tr_z) + np.mean(tr_e * tr_z) ** 2) / npts
    var_tr_z_tr_z = 2 * np.mean(tr_z * tr_z) ** 2 / npts

    samp_n_z = norm(np.mean(tr_n * tr_z), np.sqrt(var_tr_n_tr_z)).rvs(N_SAMPLES)
    samp_e_z = norm(np.mean(tr_e * tr_z), np.sqrt(var_tr_e_tr_z)).rvs(N_SAMPLES)
    samp_z_z = norm(np.mean(tr_z * tr_z), np.sqrt(var_tr_z_tr_z)).rvs(N_SAMPLES)
    samp_i = np.arctan2(np.sqrt(samp_n_z ** 2 + samp_e_z ** 2), samp_z_z) * 180 / np.pi
    incidence_uncertainty = np.std(samp_i)

    return incidence, incidence_uncertainty, azimuth, azimuth_uncertainty, coherence


def compute_azimuthal_parameters(stream):
    # computes the azimuthal parameters of a stream cut around a nice P-wave

    trace_list = _prepare_trace_list(stream)

    incidence, incidence_uncertainty, azimuth, azimuth_uncertainty, coherence = \
        compute_incidence_azimuth_uncertainty_coherence(trace_list[0], trace_list[1], trace_list[2])
    return incidence, incidence_uncertainty, azimuth, azimuth_uncertainty, coherence


def _prepare_trace_list(stream):
    tr_z = stream.select(channel='HHZ').traces[0].data
    tr_n = stream.select(channel='HHN').traces[0].data
    tr_e = stream.select(channel='HHE').traces[0].data

    return [tr_z, tr_n, tr_e]


def compute_azimuthal_parameters_flinn(stream):
    trace_list = _prepare_trace_list(stream)
    az, inc, rect, plan = pol.flinn(trace_list, noise_thres=0.0)

    return az, inc, rect, plan


def compute_azimuthal_parameters_pm(stream):
    trace_list = _prepare_trace_list(stream)
    az, inc, az_err, inc_err = pol.particle_motion_odr(trace_list, noise_thres=0.0)

    return az, inc, az_err, inc_err


def compute_azimuthal_parameters_pm_for_stream_list(stream_list):
    az_list = []
    inc_list = []
    az_err_list = []
    inc_err_list = []
    for st in stream_list:
        az, inc, az_err, inc_err = compute_azimuthal_parameters_pm(st)
        az_list.append(az)
        inc_list.append(inc)
        az_err_list.append(az_err)
        inc_err_list.append(inc_err)
    return az_list, inc_list, az_err_list, inc_err_list


def compute_azimuthal_parameters_flinn_for_stream_list(stream_list):
    az_list = []
    inc_list = []
    rect_list = []
    plan_list = []
    for st in stream_list:
        az, inc, rect, plan = compute_azimuthal_parameters_flinn(st)
        az_list.append(az)
        inc_list.append(inc)
        rect_list.append(rect)
        plan_list.append(plan)
    return az_list, inc_list, rect_list, plan_list


def compute_azimuthal_parameters_for_stream_list(stream_list):
    i_angle_list = []
    i_unc_list = []
    azimuth_list = []
    az_unc_list = []
    coh_list = []
    for st in stream_list:
        i, i_unc, az, unc, coh = compute_azimuthal_parameters(st)
        i_angle_list.append(i)
        i_unc_list.append(i_unc)
        azimuth_list.append(az)
        az_unc_list.append(unc)
        coh_list.append(coh)
    return i_angle_list, i_unc_list, azimuth_list, az_unc_list, coh_list


def get_incidence_angle(st):

    z_data = st.select(component="Z").traces[0].data
    n_data = st.select(component="N").traces[0].data
    e_data = st.select(component="E").traces[0].data

    min_max_z = max(z_data) - min(z_data)
    min_max_n = max(n_data) - min(n_data)
    min_max_e = max(e_data) - min(e_data)

    # normalize to ensure numbers are manageable
    max_of_min_maxes = max([min_max_z, min_max_n, min_max_e])
    min_max_z = min_max_z / max_of_min_maxes
    min_max_n = min_max_n / max_of_min_maxes
    min_max_e = min_max_e / max_of_min_maxes

    min_max_horiz = np.sqrt(min_max_n ** 2 + min_max_e ** 2)
    i_angle = np.arctan2(min_max_horiz, min_max_z) * 180 / np.pi

    return i_angle
