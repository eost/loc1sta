import numpy as np
import matplotlib.pyplot as plt
from functools import reduce
from math import sqrt
from loc1sta.waveforms import stack_streams


def plot_waveforms_and_stack(stream_list_to_plot, samp_rate, zero_shift, filename):
    # extract the traces

    streams_e = [st.select(channel='HHE') for st in stream_list_to_plot]
    streams_n = [st.select(channel='HHN') for st in stream_list_to_plot]
    streams_z = [st.select(channel='HHZ') for st in stream_list_to_plot]

    panels = [['stream_z', 'stream_n', 'stream_e'],
               ['stream_z', 'stream_n', 'stream_e'],
               ['stream_z', 'stream_n', 'stream_e'],
               ['stack_z', 'stack_n', 'stack_e'],
              ]

    fig, axes = plt.subplot_mosaic(panels, figsize=(16, 8))

    plt.sca(axes['stream_z'])
    plot_streams(streams_z, samp_rate=samp_rate, zero_shift=zero_shift)
    plt.title('Z component')
    plt.gca().axis('off')

    plt.sca(axes['stack_z'])
    plot_stack(streams_z, samp_rate=samp_rate, zero_shift=zero_shift)
    plt.title('Z component stack')
    clean_axes(plt.gca())
    plt.xlabel('Time relative to P-pick (s)')

    plt.sca(axes['stream_n'])
    plot_streams(streams_n, samp_rate=samp_rate, zero_shift=zero_shift)
    plt.title('N component')
    plt.gca().axis('off')

    plt.sca(axes['stack_n'])
    plot_stack(streams_n, samp_rate=samp_rate, zero_shift=zero_shift)
    plt.title('N component stack')
    clean_axes(plt.gca())
    plt.xlabel('Time relative to P-pick (s)')

    plt.sca(axes['stream_e'])
    plot_streams(streams_e, samp_rate=samp_rate, zero_shift=zero_shift)
    plt.title('E component')
    plt.gca().axis('off')

    plt.sca(axes['stack_e'])
    plot_stack(streams_e, samp_rate=samp_rate, zero_shift=zero_shift)
    plt.title('E component stack')
    clean_axes(plt.gca())
    plt.xlabel('Time relative to P-pick (s)')

    plt.savefig(filename)


def create_time_axis(npts, samp_rate, zero_shift):
    t_axis = np.arange(npts) / samp_rate - zero_shift
    return t_axis


def plot_stack(streams_to_stack, samp_rate, zero_shift):
    stack = stack_streams(streams_to_stack)
    t_axis = create_time_axis(len(stack), samp_rate, zero_shift)
    plt.plot(t_axis, stack, 'k')


def clean_axes(ax):
    ax.get_xaxis().set_visible(True)
    ax.get_yaxis().set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)


def plot_streams(streams_to_plot, samp_rate, zero_shift):
    max_value = max([max(np.abs(st.traces[0].data)) for st in streams_to_plot])
    step = 0.2
    i = 0
    for st in streams_to_plot:
        t_axis = np.arange(st.traces[0].stats.npts) / samp_rate - zero_shift
        this_max_value = max(np.abs(st.traces[0].data))
        plt.plot(t_axis, st.traces[0].data/max_value + i * step, color='k',
                 alpha=np.log10(this_max_value)/np.log10(max_value))
        i = i+1


def best_factors(n):
    step = 2 if n % 2 else 1
    factors = reduce(list.__add__, ([i, n//i] for i in range(1, int(sqrt(n))+1, step) if n % i == 0))
    return factors[-2], factors[-1]


def plot_table_of_streams(streams_to_plot, zero_shift, filename, xticks=[0, 1, 2], sp_times=None):
    # get an ordered list of streams
    # assumes you want to plot them all as cleanly as possible
    # assumes the list of streams is already sorted in the order you want to plot them

    n_events = len(streams_to_plot)
    nrows, ncols = best_factors(n_events)
    # check for bad aspect ratio, probably because we have a prime number
    fake_n_events = n_events
    while ncols > 3 * nrows:
        fake_n_events += 1
        nrows, ncols = best_factors(fake_n_events)
    start_times = [st.traces[0].stats.starttime + zero_shift for st in streams_to_plot]

    figure, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(ncols * 4, nrows * 4))

    irow = 0
    icol = 0
    for i in range(n_events):
        st = streams_to_plot[i]
        npts = st.traces[0].stats.npts
        delta = st.traces[0].stats.delta

        t_axis = np.arange(npts) * delta - zero_shift

        z_data = st.select(component="Z").traces[0].data
        n_data = st.select(component="N").traces[0].data
        e_data = st.select(component="E").traces[0].data

        offsets = {'z': (max(e_data) - min(e_data) + max(n_data) - min(e_data) + max(z_data) - min(z_data)) / 2,
                   'n': (max(e_data) - min(e_data) + max(n_data) - min(n_data)) / 2,
                   'e': 0}

        if nrows == 1:
            ax = axes[icol]
        else:
            ax = axes[irow, icol]
        plt.sca(ax)
        plt.plot(t_axis, z_data + offsets['z'], 'gray')
        plt.plot(t_axis, n_data + offsets['n'], 'gray')
        plt.plot(t_axis, e_data + offsets['e'], 'gray')
        plt.text(min(t_axis), offsets['z'], 'z', weight='bold')
        plt.text(min(t_axis), offsets['n'], 'n', weight='bold')
        plt.text(min(t_axis), offsets['e'], 'e', weight='bold')

        xlabels = [f'{x:1.1f}s' for x in xticks]

        ax.set_xticks(xticks)
        ax.set_xticklabels(xlabels)
        plt.axvline(x=0, linestyle='--', color='blue')
        if sp_times is not None:
            plt.axvline(x=sp_times[i], linestyle='--', color='red')
            plt.text(0, offsets['z'] + max(z_data)/2, f'S-P = {sp_times[i]:1.2f}s', weight='bold')
        ax.get_yaxis().set_visible(False)

        plt.title(start_times[i].isoformat())

        icol += 1
        if (icol == ncols):
            icol = 0
            irow += 1

    figure.savefig(filename)
