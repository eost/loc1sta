import os
import numpy as np
from obspy.signal.cross_correlation import xcorr_3c
from obspy.core import Stats, Trace, Stream
from obspy import UTCDateTime

NPTS = 1000

DEF_BUFFER_1 = 120  # time in s to buffer for first reading and filtering data
DEF_MIN_FREQ = 5.0  # high-pass filter value
DEF_BUFFER_P = 1.0  # time in s to buffer the P-pick for determining SNR
DEF_CUT_LEFT = 2.0  # time in seconds before the P-pick for the final cut
DEF_CUT_RIGHT = 5.0  # time in seconds after the P-pick for the final cut
DEF_MIN_SNR = 3.0  # minimum signal to noise ratio to keep a trace
DEF_SHIFT_LEN = 2


def setup_sds_system(SDSdir, stream, year, START_TIME, END_TIME):
    for tr in stream:
        # create directory for the data
        net, sta, locid, cha = tr.id.split('.')
        path_base = os.path.join(SDSdir, f'{year}', net, sta, cha + '.D')
        os.makedirs(path_base, exist_ok=True)
        print(path_base)

        # write the data to the appropriate directory
        start_time = START_TIME
        while start_time < END_TIME:
            end_time = start_time + 24 * 60 * 60
            filename = f'{path_base}/{tr.id}.D.{start_time.year}.{start_time.julday:03d}'
            print(filename)
            slice = tr.slice(start_time, end_time)
            slice.write(filename, format='MSEED')
            start_time = end_time


def min_signal_to_noise(stream, reference_time_start, reference_time_end):
    traces = stream.traces
    snr_values = []
    for tr in traces:
        std = np.std(tr.data)
        short_tr = tr.slice(starttime=reference_time_start, endtime=reference_time_end)
        max_value = max(np.abs(short_tr.data))
        signal_to_noise_ratio = max_value / std
        snr_values.append(signal_to_noise_ratio)
    if len(snr_values) < 3:
        return 0.0
    else:
        return min(snr_values)



def max_amplitude(stream):
    traces = stream.traces
    max_amps = []
    for tr in traces:
        max_value = max(np.abs(tr.data))
        max_amps.append(max_value)
    return max(max_amps)


def extract_waveforms_given_picks(picks, obspy_client, network, station, channel, location,
                                  buffer_1=DEF_BUFFER_1, min_freq=DEF_MIN_FREQ, buffer_p=DEF_BUFFER_P,
                                  cut_left=DEF_CUT_LEFT, cut_right=DEF_CUT_RIGHT, min_snr=DEF_MIN_SNR):

    n_picks = len(picks)
    good_data_flags = np.zeros(n_picks, dtype=int)
    streams = []

    for i in range(n_picks):
        pick = picks[i]
        utc_pick = UTCDateTime(pick)
        st = obspy_client.get_waveforms(network=network, station=station, channel=channel, location=location,
                                        starttime=utc_pick - buffer_1, endtime=utc_pick + buffer_1)
        st.detrend()
        st.taper(max_percentage=0.05)
        st.filter("highpass", freq=min_freq, zerophase=True)
        snr = min_signal_to_noise(st, reference_time_start=utc_pick - buffer_p,
                                  reference_time_end=utc_pick + buffer_p)
        if snr > min_snr:
            streams.append(st.trim(starttime=utc_pick - cut_left, endtime=utc_pick + cut_right))
            good_data_flags[i] = 1

    return streams, good_data_flags


def create_correlation_matrix(streams, shift_len=DEF_SHIFT_LEN):

    n_signals = len(streams)
    corr_matrix = np.zeros((n_signals, n_signals), dtype=float)

    for(i_st, st1) in enumerate(streams):
        for(j_st, st2) in enumerate(streams):
            i_shift, value = xcorr_3c(st1, st2, shift_len, abs_max=False)
            corr_matrix[i_st, j_st] = value
    return corr_matrix


def stack_streams(streams_to_stack):
    data_list = [st.traces[0].data for st in streams_to_stack]
    try:
        stack = np.sum(data_list, axis=0)
    except ValueError:
        min_len = min([len(data) for data in data_list])
        cut_data_list = [data[0:min_len-1] for data in data_list]
        stack = np.sum(cut_data_list, axis=0)
    return stack


def ricker(sigma):
    t = np.linspace(start=-6*sigma, stop=6*sigma, num=NPTS)
    f = 2 / (np.sqrt(3*sigma) * np.sqrt(np.sqrt(np.pi))) * (1 - (t/sigma)**2) * np.exp(-t**2 / (2 * sigma**2))
    return t, f

def harmonic_signal(sigma):
    t = np.linspace(start=-6*sigma, stop=6*sigma, num=NPTS)
    f = np.sin(0.5 * np.pi * t)
    return t, f

def create_p_wave(sigma, azimuth, plunge):
    t, p_wave = ricker(sigma=sigma)

    p_wave_z = p_wave * np.cos((90-plunge) * np.pi / 180.0)
    p_wave_horiz = p_wave * np.cos(plunge * np.pi / 180.0)

    azimuth_s = 180 + azimuth
    p_wave_n = p_wave_horiz * np.sin((90 - azimuth_s) * np.pi / 180.0)
    p_wave_e = p_wave_horiz * np.cos((90 - azimuth_s) * np.pi / 180.0)

    stats = Stats()
    stats.network = 'NN'
    stats.station = 'SYN'
    stats.delta = t[1]-t[0]
    stats.npts = len(t)

    stats_z = stats.copy()
    stats_z.channel = 'HHZ'
    trace_z = Trace(data=p_wave_z, header=stats_z)

    stats_n = stats.copy()
    stats_n.channel = 'HHN'
    trace_n = Trace(data=p_wave_n, header=stats_n)

    stats_e = stats.copy()
    stats_e.channel = 'HHE'
    trace_e = Trace(data=p_wave_e, header=stats_e)

    stream = Stream([trace_z, trace_n, trace_e])
    return stream


def create_noisy_P_wave(sigma, azimuth, plunge, noise_amp=0, horiz=False, vert=False):
    p_stream = create_p_wave(sigma, azimuth, plunge)
    t, noise = harmonic_signal(sigma)

    if vert:
        p_stream.select(component='Z').traces[0].data += noise * noise_amp
    if horiz:
        p_stream.select(component='E').traces[0].data += noise * noise_amp
        p_stream.select(component='N').traces[0].data += noise * noise_amp

    return p_stream

