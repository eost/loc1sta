import numpy as np
import pandas as pd
import os
import glob
from pyproj import Proj

P_PHASES = ["p", "Pg", "Pn", "P", "Pdiff", "PKP", "PKIKP"]
S_PHASES = ["s", "Sg", "Sn", "S", "Sdiff", "SKS"]

N_DIST_STEPS = 100


def get_sp_time_differences_and_angles(model, min_dist_deg, max_dist_deg, source_depth_km, receiver_depth_km=0):
    distances_deg = np.linspace(min_dist_deg, max_dist_deg, N_DIST_STEPS)
    sp_times_list = []
    incident_angle_list = []
    takeoff_angle_list = []
    for d in distances_deg:
        p_times = model.get_travel_times(source_depth_in_km=source_depth_km, distance_in_degree=d, phase_list=P_PHASES,
                                         receiver_depth_in_km=receiver_depth_km)
        s_times = model.get_travel_times(source_depth_in_km=source_depth_km, distance_in_degree=d, phase_list=S_PHASES,
                                         receiver_depth_in_km=receiver_depth_km)
        p_index = np.argmin([arrival.time for arrival in p_times])
        s_index = np.argmin([arrival.time for arrival in s_times])
        first_p_arrival_time = p_times[p_index].time
        first_s_arrival_time = s_times[s_index].time

        sp_times_list.append(first_s_arrival_time - first_p_arrival_time)
        incident_angle_list.append(p_times[p_index].incident_angle)
        takeoff_angle_list.append(p_times[p_index].takeoff_angle)
    sp_times = np.array(sp_times_list)
    incident_angles = np.array(incident_angle_list)
    takeoff_angles = np.array(takeoff_angle_list)
    return distances_deg, sp_times, incident_angles, takeoff_angles


def save_sp_time_differences_and_angles(distances_deg, sp_times, incident_angles, takeoff_angles, source_depth, dirname,
                                        basename):
    npts = len(distances_deg)
    depths = np.ones(npts) * source_depth
    data = {'depth': depths, 'dist_deg': distances_deg, 'dist_km': distances_deg * 111.25, 'sp_time': sp_times,
            'incident_angle': incident_angles, 'takeoff_angle': takeoff_angles}
    df = pd.DataFrame.from_dict(data=data)
    filename = os.path.join(dirname, basename + '_' + f'{source_depth}km' + '.csv')
    df.to_csv(filename)

def load_sp_time_differences_and_angles(dirname, basename):
    df_dict = {}
    fnames = glob.glob(os.path.join(dirname, basename + '*.csv'))
    for fname in fnames:
        df = pd.read_csv(fname)
        depth = df['depth'][0]
        df_dict[depth] = df
    return dict(sorted(df_dict.items()))

def get_distances_from_sp_times(sp_measured, sp_from_model, distances_from_model):
    return np.interp(sp_measured, sp_from_model, distances_from_model)


def get_distances_and_angles_as_function_of_depth_from_model_data(model_data_dict, sp_measured):
    dists = []
    angles = []
    depths = []
    takeoff_angles = []

    for key, ep_df in model_data_dict.items():
        dist = np.interp(sp_measured, xp=ep_df['sp_time'].values, fp=ep_df['dist_km'].values)
        angle = np.interp(sp_measured, xp=ep_df['sp_time'].values, fp=ep_df['incident_angle'].values)
        takeoff_angle = np.interp(sp_measured, xp=ep_df['sp_time'].values, fp=ep_df['takeoff_angle'].values)
        depths.append(key)
        dists.append(dist)
        angles.append(angle)
        takeoff_angles.append(takeoff_angle)
    return depths, dists, angles, takeoff_angles


def get_location_from_sta_dist_km_azimuth(sta_lat, sta_lon, dist_km, azimuth, utm_zone=39):
    p = Proj(proj='utm', zone=utm_zone, ellps='WGS84', preserve_units=False)

    x0, y0 = p(sta_lon, sta_lat)

#   Olivier's original formulation which is false I think
#    x = dist_km * 1000 * np.cos((azimuth - 90) * np.pi / 180) + x0
#    y = dist_km * 1000 * np.sin((azimuth - 90) * np.pi / 180) + y0

    x = dist_km * 1000 * np.cos((90 - azimuth) * np.pi / 180) + x0
    y = dist_km * 1000 * np.sin((90 - azimuth) * np.pi / 180) + y0


    eq_lon, eq_lat = p(x, y, inverse=True)
    return eq_lat, eq_lon


