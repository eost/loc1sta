import dateutil
import pandas as pd
import numpy as np

# row and column descriptions for standard formats
SNUFFLER_ROW_SKIP = 1
SNUFFLER_COLS = ['x0', 'Date', 'Time', 'x1', 'Channel', 'x2', 'x3', 'x4', 'Phase', 'x5', 'x6']
SNUFFLER_COLS_KEEP = ['UTC_Time', 'Channel', 'Phase']

def read_snuffler_event_markers(filename):
    df = pd.read_csv(filename, skiprows=SNUFFLER_ROW_SKIP, names=SNUFFLER_COLS, delim_whitespace=True)
    df['Otime'] = df.apply(lambda x: dateutil.parser.parse(x['Date']+'T'+x['Time']), axis=1)

    df_keep = df['Otime'].copy()
    return df_keep


def read_snuffler_phase_times(filename, skiprows=SNUFFLER_ROW_SKIP):
    df = pd.read_csv(filename, skiprows=skiprows, names=SNUFFLER_COLS, delim_whitespace=True)
#   df['UTC_Time'] = df.apply(lambda x: dateutil.parser.parse(x['Date']+'T'+x['Time']), axis=1)
    df['UTC_Time'] = df.apply(lambda x: x['Date']+'T'+x['Time'], axis=1)

    df_keep = df[SNUFFLER_COLS_KEEP].copy()
    return df_keep


def read_cluster_spreadsheet(filename, high_snr_only=False):
    cluster_dict = {}
    df = pd.read_excel(filename)

    indexes = np.unique(df['Cluster'].values)
    cleaned_indexes = [i for i in indexes if not np.isnan(i)]

    for i in cleaned_indexes:
        if high_snr_only:
            sub_df = df.loc[(df['Cluster'] == i) & (df['SNR'] == 'high')]
        else:
            sub_df = df.loc[df['Cluster'] == i]
        if not sub_df.empty:
            cluster_dict[i] = sub_df
    return cluster_dict
